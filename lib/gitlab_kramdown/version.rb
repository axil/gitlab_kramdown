# frozen_string_literal: true

module GitlabKramdown
  VERSION = '0.11.0'
end
